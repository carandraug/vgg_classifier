////////////////////////////////////////////////////////////////////////////
//    File:        preproc.h
//    Author:      Ken Chatfield
//    Description: Preprocessing functions
////////////////////////////////////////////////////////////////////////////

#ifndef CPUVISOR_UTILS_PREPROC_H_
#define CPUVISOR_UTILS_PREPROC_H_

#include <cstdint>
#include <string>
#include <limits>

#include <boost/filesystem.hpp>

#include "directencode/caffe_encoder.h"


namespace vic {
  boost::filesystem::path
  path_with_idx_suffix(const boost::filesystem::path& path,
                       const uint64_t start_idx,
                       const uint64_t end_idx);
}



namespace cpuvisor {
  void procTextFile(const std::string& text_path,
                    const std::string& proto_path,
                    featpipe::CaffeEncoder& encoder,
                    const std::string& base_path = std::string(),
                    const uint64_t start_idx = 0,
                    const uint64_t end_idx = std::numeric_limits<uint64_t>::max());
}

#endif
