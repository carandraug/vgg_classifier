#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <glog/logging.h>
#include <gflags/gflags.h>

#include "server/util/io.h"
#include "cpuvisor_config.pb.h"

#include "server/zmq_server.h"

int main(int argc, char* argv[]) {

  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
  google::ParseCommandLineFlags(&argc, &argv, true);
  if (argc < 2)
    LOG(FATAL) << "Missing required argument CONFIG-PATH";

  cpuvisor::Config config;
  cpuvisor::readProtoFromTextFile(argv[argc-1], &config);

  cpuvisor::ZmqServer zmq_server(config);
  zmq_server.serve();

  return 0;

}
