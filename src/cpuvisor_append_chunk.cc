#include <glog/logging.h>
#include <gflags/gflags.h>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
#include <boost/lexical_cast.hpp>

#include "server/util/io.h"

#include "cpuvisor_config.pb.h"


DEFINE_string(chunk_file, "", "Chunk to append to chunk index file");

int main(int argc, char* argv[]) {

  google::InitGoogleLogging(argv[0]);

  google::InstallFailureSignalHandler();
  google::SetUsageMessage("Dataset feature file chunk appender for CPU Visor server");
  google::ParseCommandLineFlags(&argc, &argv, true);
  if (argc < 2)
    LOG(FATAL) << "Missing required argument CONFIG-PATH";

  cpuvisor::Config config;
  cpuvisor::readProtoFromTextFile(argv[argc-1], &config);

  const cpuvisor::PreprocConfig& preproc_config = config.preproc_config();
  const std::string& feats_file = preproc_config.dataset_feats_file();

  // load the chunck to be appened
  fs::path chunk_file_fs(FLAGS_chunk_file);
  LOG(INFO) << "Loading chunk: " << FLAGS_chunk_file;
  cv::Mat feats_chunk;
  std::vector<std::string> paths_chunk;
  CHECK(cpuvisor::readFeatsFromProto(FLAGS_chunk_file, &feats_chunk, &paths_chunk));

  // initialize number of features and dimension
  size_t feat_num = 0, feat_dim = 0;
  feat_num = feats_chunk.rows;
  feat_dim = feats_chunk.cols;
  CHECK_EQ(feat_num, paths_chunk.size());

  // check if chunck index file exists
  if (!fs::exists(feats_file)) {

    // create the index file with using the file specified in the parameter
    LOG(INFO) << "Creating chunk index file: " << feats_file;
    std::vector<std::string> chunk_fnames;
    chunk_fnames.push_back( chunk_file_fs.filename().string() ); // save filenames only (with no path - as assumed in same directory as index file)
    cpuvisor::writeChunkIndexToProto(chunk_fnames, feat_num, feat_dim, feats_file);
  }
  else {

    // Load previous chunck file
    LOG(INFO) << "Loading chunk index file: " << feats_file;
    cpuvisor::FeatsProto feats_proto;
    bool success = readProtoFromBinaryFile(feats_file, &feats_proto);
    if  (!success)  {
        LOG(INFO) << "Could not load chunk index file: " << feats_file;
        return success;
    }
    else {

        // extract the information of the previous chunks
        size_t chunks_count = feats_proto.chunks_size();
        feat_num = feat_num + feats_proto.num();
        std::vector<std::string> chunk_fnames(chunks_count);
        for (size_t ci = 0; ci < chunks_count; ++ci) {
            chunk_fnames[ci] = feats_proto.chunks(ci);
        }
        // add the new file at the end
        chunk_fnames.push_back( chunk_file_fs.filename().string() ); // save filename only (with no path - as assumed in same directory as index file)
        LOG(INFO) << "Updating chunk index file: " << feats_file;
        cpuvisor::writeChunkIndexToProto(chunk_fnames, feat_num, feat_dim, feats_file);
    }

  }

  return 0;

}
