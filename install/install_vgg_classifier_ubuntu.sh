#!/bin/bash
set -x

## This script is to be run in a clean Ubuntu Bionic (18.04 LTS)
## machine, by a sudoer user.  Caffe is compiled for CPU use only.

# Install dependencies
sudo apt-get update
sudo apt-get install -y \
    git \
    g++ \
    cmake \
    libboost-thread-dev \
    libboost-system-dev \
    libboost-filesystem-dev \
    liblinear-dev \
    libopencv-dev \
    libcaffe-cpu-dev \
    libgoogle-glog-dev \
    libgflags-dev \
    libprotobuf-dev \
    protobuf-compiler \
    libzmq3-dev

# Dependencies for pyclient
sudo apt-get install -y \
    python3-gevent \
    python3-matplotlib \
    python3-protobuf \
    python3-zmq

# These are actually dependencies of libcaffe-cpu-dev but they are not
# declared (a packaging bug I guess).
sudo apt-get install -y \
    libhdf5-dev \
    liblmdb-dev \
    libleveldb-dev \
    libboost-python-dev

# cpp-netlib build dependencies
sudo apt-get install -y libssl-dev

# Need curl (or wget) to download releases
sudo apt-get install -y curl


# Download, build, and install cpp-netlib
curl --remote-name http://downloads.cpp-netlib.org/0.13.0/cpp-netlib-0.13.0-final.tar.gz
md5sum --check <(printf "002b0922bc7028d585c4975db748399d  cpp-netlib-0.13.0-final.tar.gz\n")
tar -xzf cpp-netlib-0.13.0-final.tar.gz
mkdir cpp-netlib-0.13.0-final/build
cd cpp-netlib-0.13.0-final/build
cmake .. \
    -DCPP-NETLIB_BUILD_TESTS=OFF \
    -DCPP-NETLIB_BUILD_EXAMPLES=OFF \
    -DCMAKE_INSTALL_PREFIX=/usr/local \
    -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=True
make
sudo make install
cd ../..
rm -r \
    cpp-netlib-0.13.0-final \
    cpp-netlib-0.13.0-final.tar.gz


# Download and build vgg-classifier
git clone -o upstream https://gitlab.com/vgg/vgg_classifier.git vgg-classifier
mkdir vgg-classifier/build
cd vgg-classifier/build
cmake ..
make
sudo make install
cd ../..
rm -r vgg-classifier/build
